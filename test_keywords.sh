#!/bin/sh

echo "Create category"
CATID=$(curl --silent -X POST -H "Content-Type: application/json" -d "{\"name\": \"testcategory\"}" http://localhost:8000/keywords | grep -o  '[0-9]\+')
echo $CATID
echo

echo "Create subcategory"
SUBCATID=$(curl --silent -X POST -H "Content-Type: application/json" -d "{\"name\": \"testsubcategory\"}" http://localhost:8000/keywords/$CATID/subcategories | grep -o  '[0-9]\+')
echo $SUBCATID
echo

echo "Create"
ID=$(curl --silent -X POST -H "Content-Type: application/json" -d "{\"name\": \"testsubcategory1\"}" http://localhost:8000/keywords/$CATID/subcategories/$SUBCATID/keywords | grep -o  '[0-9]\+')
echo $ID
echo

echo "Read"
curl -X GET -H "Accept: application/json" http://localhost:8000/keywords/$CATID/subcategories/$SUBCATID/keywords/$ID
echo

echo "Read all"
curl -X GET -H "Accept: application/json" http://localhost:8000/keywords/$CATID/subcategories/$SUBCATID/keywords
echo

echo "Update"
curl -X PUT -H "Content-Type: application/json" -d "{\"name\": \"testcategory1-modified\"}" http://localhost:8000/keywords/$CATID/subcategories/$SUBCATID/keywords/$ID
echo
curl -X GET -H "Accept: application/json" http://localhost:8000/keywords/$CATID/subcategories/$SUBCATID/keywords/$ID
echo

echo "Delete"
curl -X DELETE -H "Content-Type: application/json" http://localhost:8000/keywords/$CATID/subcategories/$SUBCATID/keywords/$ID
echo

echo "Create"
ID=$(curl --silent -X POST -H "Content-Type: application/json" -d "{\"name\": \"testsubcategory1\"}" http://localhost:8000/keywords/$CATID/subcategories/$SUBCATID/keywords | grep -o  '[0-9]\+')
echo $ID
echo

echo "Delete category (with subcategory)"
curl -X DELETE -H "Content-Type: application/json" http://localhost:8000/keywords/$CATID
echo

echo "Show categories"
curl -X GET -H "Content-Type: application/json" http://localhost:8000/keywords
