#!/bin/sh

curl -X GET -H "Accept: application/json" http://localhost:8000/search?keyword=porsche
echo

curl -X GET -H "Accept: application/json" http://localhost:8000/url/45151742504
echo

curl -X GET -H "Accept: application/json" http://localhost:8000/url/45151742504/Thumbnail
echo

curl -X GET -H "Accept: application/json" http://localhost:8000/sizes/45151742504
echo

curl -X GET -H "Accept: application/json" http://localhost:8000/keywords
echo

ID=$(curl -X POST -H "Content-Type: application/json" -d "{\"name\": \"testcategory1\"}" http://localhost:8000/keywords | grep -o  '[0-9]\+')
echo

curl -X GET -H "Accept: application/json" http://localhost:8000/keywords/$ID
echo

curl -X PUT -H "Content-Type: application/json" -d "{\"name\": \"testcategory1-modified\"}" http://localhost:8000/keywords/$ID
echo

curl -X GET -H "Accept: application/json" http://localhost:8000/keywords/$ID
echo

curl -X DELETE -H "Content-Type: application/json" http://localhost:8000/keywords/$ID
echo


