CREATE TABLE categories
(
  id SERIAL PRIMARY KEY NOT NULL,
  name varchar(255) NOT NULL
);
CREATE UNIQUE INDEX categories_id_uindex ON public.categories (id);

CREATE TABLE subcategories
(
  id SERIAL PRIMARY KEY NOT NULL,
  category_id integer,
  name varchar(255),
  CONSTRAINT subcategories_categories_id_fk FOREIGN KEY (category_id) REFERENCES public.categories (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX subcategories_id_uindex ON public.subcategories (id);

CREATE TABLE keywords
(
  id SERIAL PRIMARY KEY NOT NULL,
  subcategory_id integer,
  name varchar(255) NOT NULL,
  CONSTRAINT table_name_subcategories_id_fk FOREIGN KEY (subcategory_id) REFERENCES public.subcategories (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX keyword_id_uindex ON public.keywords (id);