## Installation

Prerequisites:
`docker`, `docker-compose`
    
    Install and start the container:
        ./install.sh
        
    Stop the container:
        ./stop.sh
        
    Uninstall and remove all data:
        ./uninstall.sh
    **Note: The uninstall script will require root privileges in order to be able to remove data.
    
Start container using script `start.sh`

Ports:

    :8000 - Application
    :8001 - PostgreSQL database
    
Accessing to database:

    database name: postgres  
    user: user  
    password: test  


## Testing

Test scripts (requires cUrl):

    ./test_flickr.sh
    ./test_categories.sh
    ./test_subcategories.sh
    ./test_keywords.sh

## Important application files

    Controllers, Crud and HTTP REST-specific classes and interfaces - www/app/Http/Controllers
    Models  - www/app/Model
    Flickr request - www/app/Flickr
    Http Request - www/app/HttpRequest

