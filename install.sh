#!/bin/sh

INSTALL_MARKER=./installed
if [ ! -f $INSTALL_MARKER ]; then

    chmod 777 -R www/bootstrap/cache
    chmod 777 -R www/storage

    docker-compose build --no-cache
    docker-compose up -d
    sleep 5
    docker-compose exec psql psql -U user -d postgres -f /postgres.sql

    touch ./installed

else

    echo "Already Installed. Use start.sh for starting container or uninstall.sh for removing containers.";

fi