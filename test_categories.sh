#!/bin/sh

echo "Create"
ID=$(curl --silent -X POST -H "Content-Type: application/json" -d "{\"name\": \"testcategory1\"}" http://localhost:8000/keywords | grep -o  '[0-9]\+')
echo $ID
echo

echo "Read"
curl -X GET -H "Accept: application/json" http://localhost:8000/keywords/$ID
echo

echo "Read all"
curl -X GET -H "Accept: application/json" http://localhost:8000/keywords
echo

echo "Update"
curl -X PUT -H "Content-Type: application/json" -d "{\"name\": \"testcategory1-modified\"}" http://localhost:8000/keywords/$ID
echo
curl -X GET -H "Accept: application/json" http://localhost:8000/keywords/$ID
echo

echo "Delete"
curl -X DELETE -H "Content-Type: application/json" http://localhost:8000/keywords/$ID
echo

echo "Show categories"
curl -X GET -H "Content-Type: application/json" http://localhost:8000/keywords
echo
