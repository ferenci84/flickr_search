#!/bin/sh

INSTALL_MARKER=./installed
if [ -f $INSTALL_MARKER ]; then

    docker-compose down
    docker-compose rm -f

    # Using root privileges because these files are added by other users
    sudo rm -R ./database_data
    sudo rm ./www/storage/framework/sessions/*
    sudo rm ./www/storage/framework/views/*
    sudo rm ./www/storage/logs/*
    sudo rm ./www/bootstrap/cache/*

    rm ./installed

else

    echo "Not installed"

fi