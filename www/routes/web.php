<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/search','SearchController@search');
Route::get('/url/{id}','SearchController@getUrl');
Route::get('/url/{id}/{size_label}','SearchController@getUrl');
Route::get('/sizes/{id}','SearchController@getSizes');

Route::get('/keywords','CategoryController@getAll');
Route::get('/keywords/{id}','CategoryController@get');
Route::post('/keywords','CategoryController@create');
Route::put('/keywords/{id}','CategoryController@update');
Route::delete('/keywords/{id}','CategoryController@delete');

Route::get('/keywords/{categoryId}/subcategories','SubcategoryController@getAll');
Route::get('/keywords/{categoryId}/subcategories/{id}','SubcategoryController@get');
Route::post('/keywords/{categoryId}/subcategories','SubcategoryController@create');
Route::put('/keywords/{categoryId}/subcategories/{id}','SubcategoryController@update');
Route::delete('/keywords/{categoryId}/subcategories/{id}','SubcategoryController@delete');


Route::get('/keywords/{categoryId}/subcategories/{subcategoryId}/keywords','KeywordController@getAll');
Route::get('/keywords/{categoryId}/subcategories/{subcategoryId}/keywords/{id}','KeywordController@get');
Route::post('/keywords/{categoryId}/subcategories/{subcategoryId}/keywords','KeywordController@create');
Route::put('/keywords/{categoryId}/subcategories/{subcategoryId}/keywords/{id}','KeywordController@update');
Route::delete('/keywords/{categoryId}/subcategories/{subcategoryId}/keywords/{id}','KeywordController@delete');
