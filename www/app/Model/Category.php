<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/29/18
 * Time: 3:34 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    public function subcategories()
    {
        return $this->hasMany('App\Model\Subcategory');
    }

}
