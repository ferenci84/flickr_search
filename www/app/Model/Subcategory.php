<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 5:04 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    public $timestamps = false;

    public function keywords()
    {
        return $this->hasMany('App\Model\Keyword');
    }

}
