<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 5:07 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    public $timestamps = false;
}
