<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/29/18
 * Time: 1:18 PM
 */

namespace App\Flickr;


use App\Flickr\Exceptions\FailedException;
use App\Flickr\Exceptions\InvalidResultException;
use App\HttpRequest\HttpRequest;
use App\HttpRequest\HttpRequestException;

class FlickrRequest
{
    const STATE_INCOMPLETE = 0;
    const STATE_COMPLETE = 1;
    const STATE_SENT = 2;
    const STATE_FAIL = 3;

    private $http_request;
    private $flickr_url;
    private $state = self::STATE_INCOMPLETE;
    private $params = [];
    private $result = null;

    public function __construct($flickr_url, $flickr_api_key, HttpRequest $httpRequest)
    {
        $this->http_request = $httpRequest;
        $this->params['nojsoncallback'] = 1;
        $this->params['format'] = 'json';
        $this->params['api_key'] = $flickr_api_key;
        $this->flickr_url = $flickr_url;

        $this->state = self::STATE_INCOMPLETE;
    }

    public function setMethod($method) {
        $this->params['method'] = 'flickr.' . $method;
        $this->state = self::STATE_COMPLETE;
    }

    public function setParam($key,$value) {
        $this->params[$key] = $value;
    }

    public function send() {
        if ($this->state != self::STATE_COMPLETE) {
            throw new InvalidStateException();
        }

        try {
            $result = $this->http_request->get($this->flickr_url,$this->params);
        } catch (HttpRequestException $e) {
            error_log('HttpRequestException');
            $this->state = self::STATE_FAIL;
            throw new FailedException();
        }

        if (isset($result)) {
            $jsonResult = json_decode($result);
            if ($jsonResult != NULL) {
                $this->result = $jsonResult;
                $this->state = self::STATE_SENT;
            } else {
                error_log('Invalid Result');
                // TODO: separate exception for invalid response if needed
                throw new FailedException();
            }
        }

    }

    public function getResult() {
        if ($this->state != self::STATE_SENT) {
            throw new InvalidStateException();
        }
        return $this->result;
    }

    public function getResultPart($path,$keys) {
        if ($this->state != self::STATE_SENT) {
            throw new InvalidStateException();
        }
        $r = $this->result;
        if (!is_object($r)) {
            throw new InvalidResultException('result is not object');
        }
        foreach ($path as $key) {
            if (!isset($r->$key)) {
                throw new InvalidResultException("Key $key is not in result object");
            }
            $r = $r->$key;
        }
        $limitedResult = null;
        if (is_array($r)) {
            $limitedResult = [];
            foreach ($r as $item) {
                $limitedResult[] = $this->limitResult($item,$keys);
            }
        } else if (is_object($r)) {
            $limitedResult = $this->limitResult($r,$keys);
        } else {
            throw new InvalidResultException("Invalid type");
        }
        return $limitedResult;
     }

     private function limitResult($item, $keys){
        $limited = new \stdClass();
        foreach ($keys as $key) {
            if (isset($item->$key)) {
                $limited->$key = $item->$key;
            }
        }
        return $limited;
     }

}
