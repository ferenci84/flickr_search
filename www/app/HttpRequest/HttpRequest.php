<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/29/18
 * Time: 1:12 PM
 */

namespace App\HttpRequest;


interface HttpRequest
{
    public function get($url, $params);
}
