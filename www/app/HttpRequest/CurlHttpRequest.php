<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/29/18
 * Time: 1:14 PM
 */

namespace App\HttpRequest;


class CurlHttpRequest implements HttpRequest
{

    public function get($url, $params)
    {
        $pairs = [];
        foreach ($params as $key => $value) {
            //TODO: url encoding
            $pairs[] = $key . "=" . $value;
        }
        $url = $url . '?' . join('&',$pairs);
        error_log('url: '.$url);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'cURL Request'
        ));

        $result = curl_exec($curl);
        if ($result == false) {
            error_log('failed request');
            throw new HttpRequestException();
        }
        return $result;
    }
}
