<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 8:22 PM
 */

namespace App\Http\Controllers;


interface CrudRelationship
{
    public function setParent($parentId);

}
