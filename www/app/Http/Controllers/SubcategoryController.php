<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/29/18
 * Time: 3:23 PM
 */

namespace App\Http\Controllers;


use App\Model\Category;
use App\Model\Subcategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SubcategoryController extends Controller
{

    function fillable()
    {
        return [
            'name',
        ];
    }

    private $crud;
    private $http;

    public function __construct(Request $request)
    {
        $this->http = new HttpLaravelBridge($request);
        $this->crud = new CrudWithResponse(new CrudImplWithRelationship(
            Category::class,
            Subcategory::class,
            'subcategories',
            $this->fillable(),
            $this->http
        ));
    }

    public function getAll($categoryId) {
        $this->crud->setParent($categoryId);
        return $this->crud->getAll();
    }

    public function get($categoryId,$id) {
        $this->crud->setParent($categoryId);
        return $this->crud->get($id);
    }

    public function create($categoryId) {
        $this->crud->setParent($categoryId);
        return $this->crud->create();
    }

    public function update($categoryId,$id) {
        $this->crud->setParent($categoryId);
        return $this->crud->update($id);
    }

    public function delete($categoryId,$id) {
        $this->crud->setParent($categoryId);
        return $this->crud->delete($id);
    }

}
