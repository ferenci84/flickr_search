<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 3:44 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Exceptions\EntityNotFoundException;

class CrudImpl implements Crud
{

    private $model_class;
    private $fillable;
    private $http;

    public function __construct($model_class, $fillable, Http $http)
    {
        $this->model_class = $model_class;
        $this->fillable = $fillable;
        $this->http = $http;
    }

    public function getAll() {
        return $this->model_class::all();
    }

    public function get($id) {
        $result = $this->model_class::find($id);
        if ($result == null) throw new EntityNotFoundException();
        return $result;
    }

    public function create() {
        $item = new $this->model_class();
        $input = $this->http->getJsonBody();
        foreach($this->fillable as $field) {
            if (isset($input[$field])) {
                $item->$field = $input[$field];
            }
        }
        $item->save();
        $result = new \stdClass();
        $result->id = $item->id;
        return $result;
    }

    public function update($id) {
        $result = $this->model_class::find($id);
        if ($result == null) throw new EntityNotFoundException();
        $input = $this->http->getJsonBody();
        foreach($this->fillable as $field) {
            if (isset($input[$field])) {
                $result->$field = $input[$field];
            }
        }
        $result->save();
        return;
    }

    public function delete($id) {
        $item = $this->model_class::find($id);
        if ($item == null) throw new EntityNotFoundException();
        $item->delete();
    }

}
