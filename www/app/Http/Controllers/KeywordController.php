<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/29/18
 * Time: 3:23 PM
 */

namespace App\Http\Controllers;


use App\Model\Category;
use App\Model\Keyword;
use App\Model\Subcategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KeywordController extends Controller
{

    function fillable()
    {
        return [
            'name',
        ];
    }

    private $crud;
    private $http;

    public function __construct(Request $request)
    {
        $this->http = new HttpLaravelBridge($request);
        $this->crud = new CrudWithResponse(new CrudImplWithRelationship(
            [Category::class,Subcategory::class],
            Keyword::class,
            ['subcategories','keywords'],
            $this->fillable(),
            $this->http
        ));
    }

    public function getAll($categoryId,$subcategoryId) {
        $this->crud->setParent([$categoryId,$subcategoryId]);
        return $this->crud->getAll();
    }

    public function get($categoryId,$subcategoryId,$id) {
        $this->crud->setParent([$categoryId,$subcategoryId]);
        return $this->crud->get($id);
    }

    public function create($categoryId,$subcategoryId) {
        $this->crud->setParent([$categoryId,$subcategoryId]);
        return $this->crud->create();
    }

    public function update($categoryId,$subcategoryId,$id) {
        $this->crud->setParent([$categoryId,$subcategoryId]);
        return $this->crud->update($id);
    }

    public function delete($categoryId,$subcategoryId,$id) {
        $this->crud->setParent([$categoryId,$subcategoryId]);
        return $this->crud->delete($id);
    }

}
