<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 4:39 PM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\Exceptions\EntityNotFoundException;

class CrudWithResponse implements Crud, CrudRelationship
{
    private $subject;

    public function __construct(Crud $subject)
    {
        $this->subject = $subject;
    }

    public function setParent($parentId) {
        try {
            if ($this->subject instanceof CrudRelationship) {
                $this->subject->setParent($parentId);
            }
        } catch (EntityNotFoundException $e) {
            return response('Not Found',404);
        } catch (\Exception $e) {
            return response('Application Error',500);
        }
    }

    public function getAll()
    {
        try {
            return response()->json($this->subject->getAll());
        } catch (EntityNotFoundException $e) {
            return response('Not Found',404);
        } catch (\Exception $e) {
            return response('Application Error',500);
        }
    }

    public function get($id)
    {
        try {
            return response()->json($this->subject->get($id));
        } catch (EntityNotFoundException $e) {
            return response('Not Found',404);
        } catch (\Exception $e) {
            return response('Application Error',500);
        }
    }

    //TODO: Bad Request
    public function create()
    {
        try {
            return response()->json($this->subject->create());
        } catch (\Exception $e) {
            return response('Application Error',500);
        }
    }

    //TODO: Bad Request
    public function update($id)
    {
        try {
            $this->subject->update($id);
            return response('', 204);
        } catch (EntityNotFoundException $e) {
                return response('Not Found',404);
        } catch (\Exception $e) {
            return response('Application Error',500);
        }
    }

    public function delete($id)
    {
        try {
            $this->subject->delete($id);
            return response('',204);
        } catch (EntityNotFoundException $e) {
            return response('Not Found',404);
        } catch (\Exception $e) {
            return response('Application Error',500);
        }
    }

}
