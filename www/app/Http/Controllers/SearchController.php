<?php

namespace App\Http\Controllers;

use App\Flickr\Exceptions\FlickrException;
use App\Flickr\FlickrRequest;
use App\HttpRequest\CurlHttpRequest;
use HttpRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    private $http_request;
    private $flickr_url;
    private $flickr_api_key;

    public function __construct()
    {
        $this->http_request = new CurlHttpRequest();
        $this->flickr_api_key = config('app.flickr_api_key');
        $this->flickr_url = config('app.flickr_url');
    }

    public function search(Request $request){
        if (!$request->has('keyword')) return response('Bad Request',400);

        try {
            $flickr = new FlickrRequest($this->flickr_url, $this->flickr_api_key, $this->http_request);
            $flickr->setMethod('photos.search');
            $flickr->setParam('text',$request->get('keyword'));
            $flickr->send();
            $result = $flickr->getResultPart(['photos','photo'],['id','title']);
            return response()->json($result);
        } catch (FlickrException $e) {
            return response('exception: '.get_class($e),500);
        }

    }

    public function getUrl($id, $size_label = 'Square') {
        try {
            $flickr = new FlickrRequest($this->flickr_url, $this->flickr_api_key, $this->http_request);
            $flickr->setMethod('photos.getSizes');
            $flickr->setParam('photo_id',$id);
            $flickr->send();
            $result = $flickr->getResultPart(['sizes','size'],['label','width','height','url']);

            foreach ($result as $size) {
                if ($size->label == $size_label) {
                    return response()->json($size);
                }
            }
            return response('Not Found',404);

        } catch (FlickrException $e) {
            return response('exception: '.get_class($e),500);
        }
    }

    public function getSizes($id) {
        try {
            $flickr = new FlickrRequest($this->flickr_url, $this->flickr_api_key, $this->http_request);
            $flickr->setMethod('photos.getSizes');
            $flickr->setParam('photo_id',$id);
            $flickr->send();
            $result = $flickr->getResultPart(['sizes','size'],['label','width','height']);
            return response()->json($result);

        } catch (FlickrException $e) {
            return response('exception: '.get_class($e),500);
        }
    }

}
