<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 3:44 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Exceptions\EntityNotFoundException;
use App\Http\Controllers\Exceptions\ParentNotSetException;

class CrudImplWithRelationship implements Crud, CrudRelationship
{

    private $parent_model_class;
    private $model_class;
    private $relationship_field;
    private $fillable;
    private $http;
    private $parent;
    private $relationship;

    public function __construct($parent_model_class, $model_class, $relationship_field, $fillable, Http $http)
    {
        $this->parent_model_class = $parent_model_class;
        $this->relationship_field = $relationship_field;
        $this->model_class = $model_class;
        $this->fillable = $fillable;
        $this->http = $http;
    }

    public function setParent($parentId) {
        if (is_array($parentId)) {
            error_log('parent id is array');
            // for multiple parent relationship, array can be given as $parentId and
            // in constructor for $parent_model_class and $relationship_field
            if (!is_array($this->parent_model_class) || is_array($this->relationship_field)) {
                foreach ($parentId as $key => $notUsed) {
                    if ($key == 0)
                        $this->parent = $this->parent_model_class[$key]::find($parentId[$key]);
                    else
                        $this->parent = $this->relationship->where('id',$parentId[$key])->first();
                    if ($this->parent == null) throw new EntityNotFoundException();
                    $relationship_field = $this->relationship_field[$key];
                    $this->relationship = $this->parent->$relationship_field();
                }
            }
        } else {
            $this->parent = $this->parent_model_class::find($parentId);
            if ($this->parent == null) throw new EntityNotFoundException();
            $relationship_field = $this->relationship_field;
            $this->relationship = $this->parent->$relationship_field();
        }
    }

    public function getAll() {
        if ($this->parent == NULL) throw new ParentNotSetException();
        return $this->relationship->get();
    }

    public function get($id) {
        if ($this->parent == NULL) throw new ParentNotSetException();
        $result = $this->relationship->where('id',$id)->first();
        if ($result == null) throw new EntityNotFoundException();
        return $result;
    }

    public function create() {
        if ($this->parent == NULL) throw new ParentNotSetException();
        $item = new $this->model_class();
        $input = $this->http->getJsonBody();
        foreach($this->fillable as $field) {
            if (isset($input[$field])) {
                $item->$field = $input[$field];
            }
        }
        $this->relationship->save($item);
        $result = new \stdClass();
        $result->id = $item->id;
        return $result;
    }

    public function update($id) {
        if ($this->parent == NULL) throw new ParentNotSetException();
        $result = $this->relationship->where('id',$id)->first();
        if ($result == null) throw new EntityNotFoundException();
        $input = $this->http->getJsonBody();
        foreach($this->fillable as $field) {
            if (isset($input[$field])) {
                $result->$field = $input[$field];
            }
        }
        $this->relationship->save($result);
        return;
    }

    public function delete($id) {
        if ($this->parent == NULL) throw new ParentNotSetException();
        $item = $this->model_class::find($id);
        if ($item == null) throw new EntityNotFoundException();
        $item->delete();
    }

}
