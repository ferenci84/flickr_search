<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 4:06 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class HttpLaravelBridge implements Http
{

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getJsonBody() {
        return $this->request->all();
    }

}
