<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/29/18
 * Time: 3:23 PM
 */

namespace App\Http\Controllers;


use App\Model\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{

    function fillable()
    {
        return [
            'name',
        ];
    }

    private $crud;

    public function __construct(Request $request)
    {
        $this->http = new HttpLaravelBridge($request);
        $this->crud = new CrudWithResponse(new CrudImpl(Category::class,$this->fillable(),$this->http));
    }

    public function getAll() {
        return $this->crud->getAll();
    }

    public function get($id) {
        return $this->crud->get($id);
    }

    public function create() {
        return $this->crud->create();
    }

    public function update($id) {
        return $this->crud->update($id);
    }

    public function delete($id) {
        return $this->crud->delete($id);
    }

}
