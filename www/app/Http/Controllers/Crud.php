<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 4:37 PM
 */

namespace App\Http\Controllers;


interface Crud
{
    public function getAll();
    public function get($id);
    public function create();
    public function update($id);
    public function delete($id);
}
