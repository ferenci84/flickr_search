<?php
/**
 * Created by IntelliJ IDEA.
 * User: ferenci84
 * Date: 11/30/18
 * Time: 4:06 PM
 */

namespace App\Http\Controllers;


interface Http
{
    public function getJsonBody();
}
