#!/bin/sh

INSTALL_MARKER=./installed
if [ -f $INSTALL_MARKER ]; then

    docker-compose up -d

else

    echo "Run install.sh first please!"

fi